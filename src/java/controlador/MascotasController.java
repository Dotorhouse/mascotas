package controlador;

import entidades.Mascotas;
import facade.MascotasFacade;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.annotation.PostConstruct;

@ManagedBean(name = "mascotasController")
@ViewScoped
public class MascotasController extends AbstractController<Mascotas> {

    @EJB
    private MascotasFacade ejbFacade;

    /**
     * Initialize the concrete Mascotas controller bean. The AbstractController
     * requires the EJB Facade object for most operations.
     * <p>
     * In addition, this controller also requires references to controllers for
     * parent entities in order to display their information from a context
     * menu.
     */
    @PostConstruct
    @Override
    public void init() {
        super.setFacade(ejbFacade);
        FacesContext context = FacesContext.getCurrentInstance();
    }

    public MascotasController() {
        // Inform the Abstract parent controller of the concrete Mascotas?cap_first Entity
        super(Mascotas.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
    }

    /**
     * Sets the "items" attribute with a collection of Categoria entities that
     * are retrieved from Mascotas?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Categoria page
     */
    public String navigateCategoriaCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Categoria_items", this.getSelected().getCategoriaCollection());
        }
        return "/categoria/index";
    }

}
