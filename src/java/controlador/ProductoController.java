package controlador;

import entidades.Producto;
import facade.ProductoFacade;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.annotation.PostConstruct;

@ManagedBean(name = "productoController")
@ViewScoped
public class ProductoController extends AbstractController<Producto> {

    @EJB
    private ProductoFacade ejbFacade;
    private CategoriaController idCategoriaController;

    /**
     * Initialize the concrete Producto controller bean. The AbstractController
     * requires the EJB Facade object for most operations.
     * <p>
     * In addition, this controller also requires references to controllers for
     * parent entities in order to display their information from a context
     * menu.
     */
    @PostConstruct
    @Override
    public void init() {
        super.setFacade(ejbFacade);
        FacesContext context = FacesContext.getCurrentInstance();
        idCategoriaController = context.getApplication().evaluateExpressionGet(context, "#{categoriaController}", CategoriaController.class);
    }

    public ProductoController() {
        // Inform the Abstract parent controller of the concrete Producto?cap_first Entity
        super(Producto.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        idCategoriaController.setSelected(null);
    }

    /**
     * Sets the "items" attribute with a collection of Ordenes entities that are
     * retrieved from Producto?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Ordenes page
     */
    public String navigateOrdenesCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Ordenes_items", this.getSelected().getOrdenesCollection());
        }
        return "/ordenes/index";
    }

    /**
     * Sets the "selected" attribute of the Categoria controller in order to
     * display its data in a dialog. This is reusing existing the existing View
     * dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdCategoria(ActionEvent event) {
        if (this.getSelected() != null && idCategoriaController.getSelected() == null) {
            idCategoriaController.setSelected(this.getSelected().getIdCategoria());
        }
    }
}
