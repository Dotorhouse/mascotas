package controlador;

import entidades.Ordenes;
import facade.OrdenesFacade;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.annotation.PostConstruct;

@ManagedBean(name = "ordenesController")
@ViewScoped
public class OrdenesController extends AbstractController<Ordenes> {

    @EJB
    private OrdenesFacade ejbFacade;
    private ProductoController idProductoController;
    private VentasController idVentaController;

    /**
     * Initialize the concrete Ordenes controller bean. The AbstractController
     * requires the EJB Facade object for most operations.
     * <p>
     * In addition, this controller also requires references to controllers for
     * parent entities in order to display their information from a context
     * menu.
     */
    @PostConstruct
    @Override
    public void init() {
        super.setFacade(ejbFacade);
        FacesContext context = FacesContext.getCurrentInstance();
        idProductoController = context.getApplication().evaluateExpressionGet(context, "#{productoController}", ProductoController.class);
        idVentaController = context.getApplication().evaluateExpressionGet(context, "#{ventasController}", VentasController.class);
    }

    public OrdenesController() {
        // Inform the Abstract parent controller of the concrete Ordenes?cap_first Entity
        super(Ordenes.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        idProductoController.setSelected(null);
        idVentaController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Producto controller in order to
     * display its data in a dialog. This is reusing existing the existing View
     * dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdProducto(ActionEvent event) {
        if (this.getSelected() != null && idProductoController.getSelected() == null) {
            idProductoController.setSelected(this.getSelected().getIdProducto());
        }
    }

    /**
     * Sets the "selected" attribute of the Ventas controller in order to
     * display its data in a dialog. This is reusing existing the existing View
     * dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdVenta(ActionEvent event) {
        if (this.getSelected() != null && idVentaController.getSelected() == null) {
            idVentaController.setSelected(this.getSelected().getIdVenta());
        }
    }
}
