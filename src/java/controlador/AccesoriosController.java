package controlador;

import entidades.Accesorios;
import facade.AccesoriosFacade;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.annotation.PostConstruct;

@ManagedBean(name = "accesoriosController")
@ViewScoped
public class AccesoriosController extends AbstractController<Accesorios> {

    @EJB
    private AccesoriosFacade ejbFacade;

    /**
     * Initialize the concrete Accesorios controller bean. The
     * AbstractController requires the EJB Facade object for most operations.
     * <p>
     * In addition, this controller also requires references to controllers for
     * parent entities in order to display their information from a context
     * menu.
     */
    @PostConstruct
    @Override
    public void init() {
        super.setFacade(ejbFacade);
        FacesContext context = FacesContext.getCurrentInstance();
    }

    public AccesoriosController() {
        // Inform the Abstract parent controller of the concrete Accesorios?cap_first Entity
        super(Accesorios.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
    }

    /**
     * Sets the "items" attribute with a collection of Categoria entities that
     * are retrieved from Accesorios?cap_first and returns the navigation
     * outcome.
     *
     * @return navigation outcome for Categoria page
     */
    public String navigateCategoriaCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Categoria_items", this.getSelected().getCategoriaCollection());
        }
        return "/categoria/index";
    }

}
