package controlador;

import entidades.Categoria;
import facade.CategoriaFacade;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.annotation.PostConstruct;

@ManagedBean(name = "categoriaController")
@ViewScoped
public class CategoriaController extends AbstractController<Categoria> {

    @EJB
    private CategoriaFacade ejbFacade;
    private MascotasController idMascotaController;
    private AlimentosController idAlimentoController;
    private AccesoriosController idAccesorioController;

    /**
     * Initialize the concrete Categoria controller bean. The AbstractController
     * requires the EJB Facade object for most operations.
     * <p>
     * In addition, this controller also requires references to controllers for
     * parent entities in order to display their information from a context
     * menu.
     */
    @PostConstruct
    @Override
    public void init() {
        super.setFacade(ejbFacade);
        FacesContext context = FacesContext.getCurrentInstance();
        idMascotaController = context.getApplication().evaluateExpressionGet(context, "#{mascotasController}", MascotasController.class);
        idAlimentoController = context.getApplication().evaluateExpressionGet(context, "#{alimentosController}", AlimentosController.class);
        idAccesorioController = context.getApplication().evaluateExpressionGet(context, "#{accesoriosController}", AccesoriosController.class);
    }

    public CategoriaController() {
        // Inform the Abstract parent controller of the concrete Categoria?cap_first Entity
        super(Categoria.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        idMascotaController.setSelected(null);
        idAlimentoController.setSelected(null);
        idAccesorioController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Mascotas controller in order to
     * display its data in a dialog. This is reusing existing the existing View
     * dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdMascota(ActionEvent event) {
        if (this.getSelected() != null && idMascotaController.getSelected() == null) {
            idMascotaController.setSelected(this.getSelected().getIdMascota());
        }
    }

    /**
     * Sets the "selected" attribute of the Alimentos controller in order to
     * display its data in a dialog. This is reusing existing the existing View
     * dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdAlimento(ActionEvent event) {
        if (this.getSelected() != null && idAlimentoController.getSelected() == null) {
            idAlimentoController.setSelected(this.getSelected().getIdAlimento());
        }
    }

    /**
     * Sets the "selected" attribute of the Accesorios controller in order to
     * display its data in a dialog. This is reusing existing the existing View
     * dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdAccesorio(ActionEvent event) {
        if (this.getSelected() != null && idAccesorioController.getSelected() == null) {
            idAccesorioController.setSelected(this.getSelected().getIdAccesorio());
        }
    }

    /**
     * Sets the "items" attribute with a collection of Producto entities that
     * are retrieved from Categoria?cap_first and returns the navigation
     * outcome.
     *
     * @return navigation outcome for Producto page
     */
    public String navigateProductoCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Producto_items", this.getSelected().getProductoCollection());
        }
        return "/producto/index";
    }

}
