package controlador;

import entidades.Ventas;
import facade.VentasFacade;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.annotation.PostConstruct;

@ManagedBean(name = "ventasController")
@ViewScoped
public class VentasController extends AbstractController<Ventas> {

    @EJB
    private VentasFacade ejbFacade;
    private ClienteController idClienteController;
    private UsuariosController idUsuarioController;

    /**
     * Initialize the concrete Ventas controller bean. The AbstractController
     * requires the EJB Facade object for most operations.
     * <p>
     * In addition, this controller also requires references to controllers for
     * parent entities in order to display their information from a context
     * menu.
     */
    @PostConstruct
    @Override
    public void init() {
        super.setFacade(ejbFacade);
        FacesContext context = FacesContext.getCurrentInstance();
        idClienteController = context.getApplication().evaluateExpressionGet(context, "#{clienteController}", ClienteController.class);
        idUsuarioController = context.getApplication().evaluateExpressionGet(context, "#{usuariosController}", UsuariosController.class);
    }

    public VentasController() {
        // Inform the Abstract parent controller of the concrete Ventas?cap_first Entity
        super(Ventas.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        idClienteController.setSelected(null);
        idUsuarioController.setSelected(null);
    }

    /**
     * Sets the "items" attribute with a collection of Ordenes entities that are
     * retrieved from Ventas?cap_first and returns the navigation outcome.
     *
     * @return navigation outcome for Ordenes page
     */
    public String navigateOrdenesCollection() {
        if (this.getSelected() != null) {
            FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("Ordenes_items", this.getSelected().getOrdenesCollection());
        }
        return "/ordenes/index";
    }

    /**
     * Sets the "selected" attribute of the Cliente controller in order to
     * display its data in a dialog. This is reusing existing the existing View
     * dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdCliente(ActionEvent event) {
        if (this.getSelected() != null && idClienteController.getSelected() == null) {
            idClienteController.setSelected(this.getSelected().getIdCliente());
        }
    }

    /**
     * Sets the "selected" attribute of the Usuarios controller in order to
     * display its data in a dialog. This is reusing existing the existing View
     * dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdUsuario(ActionEvent event) {
        if (this.getSelected() != null && idUsuarioController.getSelected() == null) {
            idUsuarioController.setSelected(this.getSelected().getIdUsuario());
        }
    }
}
