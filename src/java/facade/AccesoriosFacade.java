/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import entidades.Accesorios;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author miokitsune
 */
@Stateless
public class AccesoriosFacade extends AbstractFacade<Accesorios> {
    @PersistenceContext(unitName = "MascotasTiendaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AccesoriosFacade() {
        super(Accesorios.class);
    }
    
}
