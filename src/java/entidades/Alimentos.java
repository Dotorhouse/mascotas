/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author miokitsune
 */
@Entity
@Table(name = "alimentos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Alimentos.findAll", query = "SELECT a FROM Alimentos a"),
    @NamedQuery(name = "Alimentos.findByIdAlimento", query = "SELECT a FROM Alimentos a WHERE a.idAlimento = :idAlimento"),
    @NamedQuery(name = "Alimentos.findByNombre", query = "SELECT a FROM Alimentos a WHERE a.nombre = :nombre"),
    @NamedQuery(name = "Alimentos.findByMarca", query = "SELECT a FROM Alimentos a WHERE a.marca = :marca"),
    @NamedQuery(name = "Alimentos.findByTipoAli", query = "SELECT a FROM Alimentos a WHERE a.tipoAli = :tipoAli"),
    @NamedQuery(name = "Alimentos.findByDescripcion", query = "SELECT a FROM Alimentos a WHERE a.descripcion = :descripcion"),
    @NamedQuery(name = "Alimentos.findByNombreIma", query = "SELECT a FROM Alimentos a WHERE a.nombreIma = :nombreIma")})
public class Alimentos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_alimento")
    private Integer idAlimento;
    @Size(max = 30)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 30)
    @Column(name = "marca")
    private String marca;
    @Size(max = 30)
    @Column(name = "tipo_ali")
    private String tipoAli;
    @Size(max = 50)
    @Column(name = "descripcion")
    private String descripcion;
    @Size(max = 30)
    @Column(name = "nombre_ima")
    private String nombreIma;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAlimento")
    private Collection<Categoria> categoriaCollection;

    public Alimentos() {
    }

    public Alimentos(Integer idAlimento) {
        this.idAlimento = idAlimento;
    }

    public Integer getIdAlimento() {
        return idAlimento;
    }

    public void setIdAlimento(Integer idAlimento) {
        this.idAlimento = idAlimento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getTipoAli() {
        return tipoAli;
    }

    public void setTipoAli(String tipoAli) {
        this.tipoAli = tipoAli;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombreIma() {
        return nombreIma;
    }

    public void setNombreIma(String nombreIma) {
        this.nombreIma = nombreIma;
    }

    @XmlTransient
    public Collection<Categoria> getCategoriaCollection() {
        return categoriaCollection;
    }

    public void setCategoriaCollection(Collection<Categoria> categoriaCollection) {
        this.categoriaCollection = categoriaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAlimento != null ? idAlimento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Alimentos)) {
            return false;
        }
        Alimentos other = (Alimentos) object;
        if ((this.idAlimento == null && other.idAlimento != null) || (this.idAlimento != null && !this.idAlimento.equals(other.idAlimento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Alimentos[ idAlimento=" + idAlimento + " ]";
    }
    
}
