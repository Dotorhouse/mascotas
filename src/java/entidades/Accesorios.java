/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author miokitsune
 */
@Entity
@Table(name = "accesorios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Accesorios.findAll", query = "SELECT a FROM Accesorios a"),
    @NamedQuery(name = "Accesorios.findByIdAccesorio", query = "SELECT a FROM Accesorios a WHERE a.idAccesorio = :idAccesorio"),
    @NamedQuery(name = "Accesorios.findByNombre", query = "SELECT a FROM Accesorios a WHERE a.nombre = :nombre"),
    @NamedQuery(name = "Accesorios.findByMarca", query = "SELECT a FROM Accesorios a WHERE a.marca = :marca"),
    @NamedQuery(name = "Accesorios.findByDescripcion", query = "SELECT a FROM Accesorios a WHERE a.descripcion = :descripcion"),
    @NamedQuery(name = "Accesorios.findByNombreIma", query = "SELECT a FROM Accesorios a WHERE a.nombreIma = :nombreIma")})
public class Accesorios implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_accesorio")
    private Integer idAccesorio;
    @Size(max = 30)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 30)
    @Column(name = "marca")
    private String marca;
    @Size(max = 30)
    @Column(name = "descripcion")
    private String descripcion;
    @Size(max = 30)
    @Column(name = "nombre_ima")
    private String nombreIma;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAccesorio")
    private Collection<Categoria> categoriaCollection;

    public Accesorios() {
    }

    public Accesorios(Integer idAccesorio) {
        this.idAccesorio = idAccesorio;
    }

    public Integer getIdAccesorio() {
        return idAccesorio;
    }

    public void setIdAccesorio(Integer idAccesorio) {
        this.idAccesorio = idAccesorio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombreIma() {
        return nombreIma;
    }

    public void setNombreIma(String nombreIma) {
        this.nombreIma = nombreIma;
    }

    @XmlTransient
    public Collection<Categoria> getCategoriaCollection() {
        return categoriaCollection;
    }

    public void setCategoriaCollection(Collection<Categoria> categoriaCollection) {
        this.categoriaCollection = categoriaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAccesorio != null ? idAccesorio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Accesorios)) {
            return false;
        }
        Accesorios other = (Accesorios) object;
        if ((this.idAccesorio == null && other.idAccesorio != null) || (this.idAccesorio != null && !this.idAccesorio.equals(other.idAccesorio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Accesorios[ idAccesorio=" + idAccesorio + " ]";
    }
    
}
