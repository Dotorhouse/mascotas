/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author miokitsune
 */
@Entity
@Table(name = "ordenes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ordenes.findAll", query = "SELECT o FROM Ordenes o"),
    @NamedQuery(name = "Ordenes.findByIdOrden", query = "SELECT o FROM Ordenes o WHERE o.idOrden = :idOrden"),
    @NamedQuery(name = "Ordenes.findByCantidadVen", query = "SELECT o FROM Ordenes o WHERE o.cantidadVen = :cantidadVen"),
    @NamedQuery(name = "Ordenes.findByStatus", query = "SELECT o FROM Ordenes o WHERE o.status = :status"),
    @NamedQuery(name = "Ordenes.findByDeposito", query = "SELECT o FROM Ordenes o WHERE o.deposito = :deposito"),
    @NamedQuery(name = "Ordenes.findByOrdenStatus", query = "SELECT o FROM Ordenes o WHERE o.ordenStatus = :ordenStatus"),
    @NamedQuery(name = "Ordenes.findByFechaTentativa", query = "SELECT o FROM Ordenes o WHERE o.fechaTentativa = :fechaTentativa"),
    @NamedQuery(name = "Ordenes.findByFechaEntrega", query = "SELECT o FROM Ordenes o WHERE o.fechaEntrega = :fechaEntrega"),
    @NamedQuery(name = "Ordenes.findByFechaEnvio", query = "SELECT o FROM Ordenes o WHERE o.fechaEnvio = :fechaEnvio")})
public class Ordenes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_orden")
    private Integer idOrden;
    @Column(name = "cantidad_ven")
    private Integer cantidadVen;
    @Column(name = "status")
    private Integer status;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "deposito")
    private BigDecimal deposito;
    @Column(name = "orden_status")
    private Integer ordenStatus;
    @Column(name = "fecha_tentativa")
    @Temporal(TemporalType.DATE)
    private Date fechaTentativa;
    @Column(name = "fecha_entrega")
    @Temporal(TemporalType.DATE)
    private Date fechaEntrega;
    @Column(name = "fecha_envio")
    @Temporal(TemporalType.DATE)
    private Date fechaEnvio;
    @JoinColumn(name = "id_producto", referencedColumnName = "id_producto")
    @ManyToOne(optional = false)
    private Producto idProducto;
    @JoinColumn(name = "id_venta", referencedColumnName = "id_venta")
    @ManyToOne(optional = false)
    private Ventas idVenta;

    public Ordenes() {
    }

    public Ordenes(Integer idOrden) {
        this.idOrden = idOrden;
    }

    public Integer getIdOrden() {
        return idOrden;
    }

    public void setIdOrden(Integer idOrden) {
        this.idOrden = idOrden;
    }

    public Integer getCantidadVen() {
        return cantidadVen;
    }

    public void setCantidadVen(Integer cantidadVen) {
        this.cantidadVen = cantidadVen;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BigDecimal getDeposito() {
        return deposito;
    }

    public void setDeposito(BigDecimal deposito) {
        this.deposito = deposito;
    }

    public Integer getOrdenStatus() {
        return ordenStatus;
    }

    public void setOrdenStatus(Integer ordenStatus) {
        this.ordenStatus = ordenStatus;
    }

    public Date getFechaTentativa() {
        return fechaTentativa;
    }

    public void setFechaTentativa(Date fechaTentativa) {
        this.fechaTentativa = fechaTentativa;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Date getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(Date fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public Producto getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Producto idProducto) {
        this.idProducto = idProducto;
    }

    public Ventas getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(Ventas idVenta) {
        this.idVenta = idVenta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idOrden != null ? idOrden.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ordenes)) {
            return false;
        }
        Ordenes other = (Ordenes) object;
        if ((this.idOrden == null && other.idOrden != null) || (this.idOrden != null && !this.idOrden.equals(other.idOrden))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Ordenes[ idOrden=" + idOrden + " ]";
    }
    
}
